# seven-segment-dashboard

Show various dashboard items using a 7-segment display

- ingest webhook data
- poll traffic info
- watch for visual changes
- keep 7-segment updated